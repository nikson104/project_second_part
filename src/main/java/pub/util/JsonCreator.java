package pub.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.List;
import java.util.concurrent.Callable;
import pub.entity.Beer;

public class JsonCreator implements Callable<String> {

  private List<Beer> data;

  public JsonCreator (List<Beer> data) {
    this.data = data;
  }

  @Override
  public String call() {
    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    return gson.toJson(data);
  }
}
