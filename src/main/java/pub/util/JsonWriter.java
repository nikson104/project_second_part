package pub.util;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import pub.entity.Beer;

public class JsonWriter {

  private static JsonWriter instance;

  private JsonWriter() {
  }

  public static synchronized JsonWriter getInstance() {
    if (instance == null) {
      instance = new JsonWriter();
    }
    return instance;
  }

  public void writeJsonToFile(String fileName, List<Beer> resultList) throws ExecutionException, InterruptedException {
    Callable<String> callable = new JsonCreator(resultList);
    FutureTask<String> json = new FutureTask<>(callable);
    Thread createJson = new Thread(json);
    createJson.start();
    Thread fileWriter = new Thread(new Writer(fileName, json.get()));
    fileWriter.start();
  }
}
