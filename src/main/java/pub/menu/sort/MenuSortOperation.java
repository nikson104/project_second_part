package pub.menu.sort;

import pub.entity.Beer;
import pub.entity.Pub;
import pub.menu.MenuItem;
import pub.menu.RootMenuItem;
import pub.operations.SortAction;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pub.util.JsonWriter;

public abstract class MenuSortOperation implements MenuItem {
  private static final Logger LOGGER = Logger.getLogger(MenuSortOperation.class.getName());

  protected final SortAction operation;
  protected final RootMenuItem rootMenuItem;
  protected final Pub pub;
  protected final JsonWriter jsonWriter;

  public MenuSortOperation(SortAction operation, RootMenuItem rootMenuItem, Pub pub) {
    this.rootMenuItem = rootMenuItem;
    this.operation = operation;
    this.pub = pub;
    this.jsonWriter = JsonWriter.getInstance();
  }

  public void execute() {
    try {
      List<Beer> resultList = operation.execute(pub.getBeers());
      jsonWriter.writeJsonToFile(operation.toString(), resultList);
    } catch (Exception e) {
      LOGGER.log(Level.INFO, e.getMessage(), e);
    }
  }
}