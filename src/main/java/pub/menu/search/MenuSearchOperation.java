package pub.menu.search;

import pub.entity.Beer;
import pub.entity.Pub;
import pub.menu.MenuItem;
import pub.menu.RootMenuItem;
import pub.operations.SearchAction;

import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import pub.util.JsonWriter;

public abstract class MenuSearchOperation implements MenuItem {
  private static final Logger LOGGER = Logger.getLogger(MenuSearchOperation.class.getName());

  protected static final Scanner SCANNER = new Scanner(System.in);

  protected final SearchAction operation;
  protected final RootMenuItem rootMenuItem;
  protected final Pub pub;
  protected final JsonWriter jsonWriter;

  public MenuSearchOperation(SearchAction operation, RootMenuItem rootMenuItem, Pub pub) {
    this.rootMenuItem = rootMenuItem;
    this.operation = operation;
    this.pub = pub;
    this.jsonWriter = JsonWriter.getInstance();
  }

  public void execute(Object param) {
    try {
      List<Beer> resultList = operation.execute(pub.getBeers(), param);
      jsonWriter.writeJsonToFile(operation.toString(), resultList);
    } catch (Exception e) {
      LOGGER.log(Level.INFO, e.getMessage(), e);
    }
  }
}