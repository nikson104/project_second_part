package pub.operations;

import pub.entity.Beer;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortByPrice implements SortAction<Beer> {

  @Override
  public List<Beer> execute(List<Beer> beers) {
    synchronized (beers) {
      beers.sort(Comparator.comparingInt(Beer::getPrice));
    }
    return beers;
  }
}
